
## 0.0.8 [01-31-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays!6

---

## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays!5

---

## 0.0.6 [06-14-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays!4

---

## 0.0.5 [12-03-2021]

* Update readme to current version

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays!3

---

## 0.0.4 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-objects-or-arrays!2

---

## 0.0.3 [05-14-2021]

* [LB-432] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/remove-duplicates-from-array-of-objects-or-arrays!1

---

## 0.0.2 [12-23-2020]

* [LB-432] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/remove-duplicates-from-array-of-objects-or-arrays!1

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
